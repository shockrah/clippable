# Clippable

A small self hostable alternative to streamable. Built with Rocket.rs

## Testing

A docker container is provided: 

* `docker pull registry.gitlab.com/shockrah/clippable:latest`

Below is the suggested way test things out

* `docker run --rm -p 8482:8482 -it -v /path/to/clips:/media/clips -v /path/to/thumbnails:/media/thumbnails`

**NOTE**: Without proper thumbnails you get the "404" gif everywhere but the
videos will still be there.

## Admin Setup/Configuration

There are a few environment variables that can be configured:

* `CLIPS_DIR` : default = `/media/clips`

* `THUMBS_DIR`: default = `/media/thumbnails`

* `SITE_NAME`: default = _Clippable_

* `SITE_DESC`: default = _Short clips_

* `SITE_URL`: default = `#`

## Building the backend

Currently the project relies on Rust Nightly to build due to a requirement in
Rocket however as that framework moves to Rust Stable so to will this project.

## Building frontend javascript

Currently the website's javascript is written in typescript then compiled to 
Javascript. The output directory is `/api/static/js/` which means `mkdir -p api/static/js`
must be ran in order to ensure that output directory is present and compilation
succeeds.  There are no dependencies and is only written in typescript to 
enforce some level of consistency among contributor code.

