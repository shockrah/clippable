+++
title = "Tell me more"
+++
> *Why don't I show you instead*

Check my personal instance here: [clips.shockrah.xyz](https://clips.shockrah.xyz)

# Like posting clips everywhere?

So do I, especially with friends when something interesting/funny happens.
I also don't like relying on others to host content for me so I made this to do
exactly that.

You can post whatever you want because its your content, your site, your rules.

# How does it work?

Well clips that you upload to your server are organized into categories and
served from there.  Categories can have thumbnails too so they look distinct
but that's up to you to setup.

# Can I make a script to get videos?

Sure the public API is actually really simple:

These are basically all the routes you'll need

* GET `/api/categories/list`

Lists out the categories available

* GET `/api/category/<category-name>`

Lists out the videos for that given category

* GET `/video/<category>/<file>`

Fetches the video file itself named by `file`


# How easy is it to upload videos?

Right now you just need to drop a video file into a sub-folder of your server's
videos directory. So if you had `/media/videos` then you would put a video into
a folder like `/media/videos/cool-clips/` or whatever else it's named.

Currently I'm working on making a simple clip-uploading tool that would also take
care of generating the appropriate thumbnails for you.
