+++
title = "Configuring"
+++

# Environment Variables

* `CLIPS_DIR` : default = `/media/clips`

This is the root directory for all clips that you intend on serving; it will have a structure similar to this example one.

```
vids
├── funny
│   └── another-clip.mp4
├── life
└── serious
    └── clip.mkv
```


* `THUMBS_DIR`: default = `/media/thumbnails`

This is the root directory for all clips that you intend on serving; it will have a structure similar to this example one.

* `SITE_NAME`: default = `Clippable`

* `SITE_DESC`: default = `Short Clips`

* `SITE_URL`: default = `#`

```
thumbs
├── funny
│   ├── category-thumbnail.jpg
│   └── clip.mkv.jpg
├── life
│   └── category-thumbnail.jpg
└── serious
    ├── category-thumbnail.jpg
    └── clip.mkv.jpg
```

# Rocket.toml

Below is a list of options and their default values in the provided `Rocket.toml` file.
This is generally used to configure the webserver itself and not much else.

|Option |Default Value|
|-------------|--------------|
|  address    |   0.0.0.0    |
|    port     |    8482      |
| keep_alive  |      5       |
|read_timeout |      5       |
|write_timeout|      5       |
|   log       |  critical    |
|  limits     | {forms=4096} |

