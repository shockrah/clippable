FROM debian:sid-slim

# Default daniel dirs
RUN mkdir -p /media/clips /media/thumbnails
COPY build/ /app
WORKDIR /app
ENTRYPOINT [ "/app/server" ]

