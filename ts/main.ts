import * as ready from './ready'

enum PageType {
	Index,
	Category,
	Admin,
}

function get_page_type(uri: string) : PageType {
	if(uri == '/') {
		return PageType.Index
	} 
	if (uri.startsWith('/category')) {
		return PageType.Category
	} 
	if (uri.startsWith('/admin')) {
		return PageType.Admin
	}
	return PageType.Index
}

document.addEventListener('readystatechange', (e?: Event) : Event => {
	/*
	 * This dispatch method basically figures out what kind of
	 * page we're sitting on before delegating control to the
	 * appropriate place.
	 */
	switch (get_page_type(document.location.pathname)) {
		case PageType.Index: return ready.index_ready_handler(e)
		case PageType.Category: return ready.category_ready_handler(e)
		case PageType.Admin: return ready.admin_ready_handler(e)
	}
})
