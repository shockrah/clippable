export class VideoMeta {
	name: string|null
	thumbnail: string|null
	category: string
	basename: string|null
	href: string

	constructor(raw: any) {
		this.name = raw['name']
		this.thumbnail = raw['thumbnail']

		let path = window.location.pathname
		this.category = path.substring(path.lastIndexOf('/')+1)

		this.basename = this.name ?
			this.name.slice(0, this.name.lastIndexOf('.')) :
			null

		this.href = `/clip/${this.category}/${this.basename}`
	}

	private clean_link(link: string) : string {
		// NOTE: no need to url encode since browser does that anyway
		for(const part of ['.', '-', '_', 'mp4', 'mkv', 'webm']) {
			link = link.replace(part, ' ')
		}
		return link
	}

	title_link() : HTMLHeadingElement {
		let container = document.createElement('h2')
		let link = document.createElement('a')
		if(this.name) {
			link.href = this.href
			link.text = this.clean_link(this.name)
		} else  {
			link.href = '#'
			link.text = this.name + 'ft. No thumbnail'
		}

		container.appendChild(link)


		return container
	}

	thumbnail_div() : HTMLAnchorElement {
		let nail = document.createElement('img')
		nail.className = 'img-fluid'

		// Modern browser's should be able to cache this request
		if(!(this.thumbnail == null || this.thumbnail.length == 0)) {
			nail.setAttribute('src', this.thumbnail)
		} else{
			nail.setAttribute('src', '/cantfindshit.gif')
		}
		let link = document.createElement('a')
		if(this.name) {
			link.href = `/clip/${this.category}/${this.basename}`
		} else  {
			link.href = '#'
		}
		link.appendChild(nail)

		return link
	}

	public as_div() {
		let container = document.createElement('div')
		container.className = 'video-block'

		let title = this.title_link()
		let thumbnail = this.thumbnail_div()

		container.appendChild(title)
		container.appendChild(thumbnail)

		// Dump gallery onto the main page
		document.getElementById('main-container').appendChild(container)

		return container
	}

	public as_li() : HTMLElement {
		const li = document.createElement('li')
		li.className = 'align-left list-group-item'

		const link = document.createElement('a')
		link.href = `/clip/${this.category}/${this.name}`
		link.target = '_blank'
		link.textContent = this.name
		link.className = 'admin-video-li btn'
		li.appendChild(link)

		const thumbnail_link = document.createElement('a')
		thumbnail_link.href = `/thumbnail/${this.category}/${this.name}.jpg`
		link.target = '_blank'
		thumbnail_link.innerHTML = '<i class="fa-solid fa-image"></i>'
		thumbnail_link.className = 'admin-video-li btn'
		li.appendChild(thumbnail_link)

		const delete_btn = document.createElement('button')
		delete_btn.type = 'button'
		delete_btn.className = 'btn btn-danger'
		delete_btn.innerHTML = '<i class="fa-solid fa-trash"></i>'
		li.appendChild(delete_btn)

		return li
	}
}

export async function fetch_category_videos(name?: string) : Promise<Array<VideoMeta>> {
	const category = name ? name : window.location.pathname
	const endpoint = window.location.origin + `/api/category/${category}`
	let videos: Array<VideoMeta> = []
	const response = await fetch(endpoint)
	if(response.headers.get('Content-Type') == 'application/json') {
		const raw = JSON.parse(await response.text())
		for(const vid_raw of raw) {
			videos.push(new VideoMeta(vid_raw))
		}
	}
	return videos
}

