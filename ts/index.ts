class Category {
	name: string
	thumbnail: string|null
	constructor(raw: any) {
		this.name = raw['name']
		this.thumbnail= raw['thumbnail']

		// Setting some defaults
		if(!this.name) { this.name = null }
		if(!this.thumbnail) { this.thumbnail= null }
	}

	title_link() : HTMLHeadingElement {
		let container = document.createElement('h2')

		let link = document.createElement('a')
		if(this.name) {
			link.href = `/category/${this.name}`
			link.text = this.name
		} else  {
			link.href = '#'
			link.text = this.name + 'ft. No thumbnail'
		}
		link.text = link.text[0].toUpperCase() + link.text.substring(1, link.text.length)

		container.appendChild(link)


		return container
	}

	thumbnail_div() : HTMLAnchorElement {
		let nail = document.createElement('img')
		nail.className = 'img-fluid'

		// Modern browser's should be able to cache this request
		if(!(this.thumbnail == null || this.thumbnail.length == 0)) {
			nail.setAttribute('src', `${this.thumbnail}`)
		} else{
			nail.setAttribute('src', '/static/cantfindshit.gif')
		}

		let link = document.createElement('a')
		if(this.name) {
			link.href = `/category/${this.name}`
		} else  {
			link.href = '#'
		}
		link.appendChild(nail)

		return link
	}


	public as_div() {
		let container = document.createElement('div')
		container.className = 'video-block'

		let title = this.title_link()
		let thumbnail = this.thumbnail_div()

		container.appendChild(title)
		container.appendChild(thumbnail)

		// Dump gallery onto the main page
		document.getElementById('main-container').appendChild(container)

		return container
	}
}


export async function fetch_categories() : Promise<Array<Category>> {
	const endpoint = window.location.origin + '/api/categories'

	let cats: Array<Category>  = []
	const response = await fetch(endpoint)
	if(response.headers.get('Content-Type') == 'application/json') {
		const raw = JSON.parse(await response.text())
		for(const cat_raw of raw) {
			cats.push(new Category(cat_raw))
		}
	}

	return cats
}

