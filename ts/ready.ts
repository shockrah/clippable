/*
 * This module defines the hooks required
 * to initialize a page's JS environment
 * when the document.readystate is changed
 * */
import {fetch_categories }  from './index'
import { fetch_category_videos } from './category'
import { populate_meta_form } from './admin'

export function category_ready_handler(e?: Event) : Event {
	/*
	 * On page load:
	 * 	This function takes care of fetching the links/thumbnails for that
	 * 	category
	 */
	if(document.readyState != 'complete') { return e }

	fetch_category_videos()
	.then(videos => {
		for(const vid of videos) {
			vid.as_div()
		}
	})
	.catch(reason => {
		// TODO: actual error handling
		console.log(reason)
	})
	return e
}

export function index_ready_handler(e?: Event) : Event {
	// Only let this make a get request once we're ready on the page
	if(document.readyState != 'complete') { return e }

	// All we do here is basically make a get request to /api/categories
	fetch_categories()
	.then(categories => {
		for(const cat of categories) {
			cat.as_div()
		}
	})
	.catch(reason => {
		// TODO: actual  error hanlding
		console.log('index_ready_handler::fetch_categories rejection', reason)
	})
	return e
}

export function admin_ready_handler(e?: Event) : Event {
	// Binding callbacks to frontend
	document.getElementById('video-file').onchange = populate_meta_form
	return e
}

