#!/bin/sh

set -e

# This script basically clones the tree structure of the videos directory
# At the same time it will generate thumbnails for each video

vids="$1" # root dir for videos that we're going to clone
thumbs="$2" # root dir for thumbnails we'll create

_show_usage() {
cat << EOF 
Generate thumbnails for a whole tree of clips
Usage:
    $0 CLIPS_ROOT_PATH TARGET_ROOT
EOF
exit 1
}

if [ -z "$vids" ];then 
    echo 'Missing root video directory!'
    _show_usage 
fi

if [ -z "$thumbs" ];then
    echo 'Missing target thumbnails directory'
    _show_usage
fi

echo Cloning directory structure
pushd "$vids" > /dev/null
    find . -type d | while read f; do 
        mkdir -p "$thumbs/$f"
    done
popd > /dev/null

echo Generating thumbnails
thumbs="`realpath "$thumbs"`"
pushd "$vids" > /dev/null
    find . -type f -name '*.mkv' -o -name '*.mp4' -o -name '*.webm' | while read f; do
        if="`realpath "${vids}/$f"`"
        of="`realpath "${thumbs}/$f"`"
        # Make sure only errors pop up here
        ffmpeg -hide_banner -loglevel error \
            -y -ss 00:00:01 -i "$if" -frames:v 1 "$of.jpg" > /dev/null
        ffmpeg -hide_banner -loglevel error \
            -y -i "$of.jpg" -vf scale=640:-1 "$of.jpg" > /dev/null
        echo $of
    done
popd > /dev/null