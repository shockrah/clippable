import json
import subprocess

'''
{
    'transactions': 3564,
    'availability': 100.0,
    'elapsed_time': 4.38,
    'data_transferred': 7.1,
    'response_time': 0.08,
    'transaction_rate': 813.7,
    'throughput': 1.62,
    'concurrency': 61.21,
    'successful_transactions': 3564,
    'failed_transactions': 0,
    'longest_transaction': 3.28,
    'shortest_transaction': 0.0
}
'''

def color(text: str, color: str):
    text = text.lower()
    colors = {
        'red': '\e[0;31m',
        'blue': '\e[0;34m',
        'green': '\e[0;32m'
    }
    nc = '\e[0m'
    if text not in colors:
        return text
    else:
        return colors[color] + text + nc

if __name__ == '__main__':
    print('Starting')
    proc = subprocess.run(
        'siege -q -t 5S -c 200 http://localhost/'.split(),
        text=True,
        capture_output=True
    )
    result = json.loads(proc.stdout)
    print(json.dumps(result,indent=2))

