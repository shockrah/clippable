#!/bin/sh

vid_path="$1"
nail_dir="$2"

# ./nail asd/fas/dfasdf.mkv /media/videos /media/thumbnails

show_usage() {
cat << EOF 
$@ 
./nail.sh VIDEO_FILE OUTPUT_DIR
Example:
	./nail video.mp4 /thumbnails/example
VIDEO_FILE
	Path to file from which we generate a thumbnail
OUTPUT_DIR
	Directory to save the thumbnail
EOF
}

# First some basic checks
if [ ! -f "$vid_path" ]; then
	show_usage Video file path invalid
	exit 1
fi


if [ ! -d "$nail_dir" ];then
	show_usage Thumbnails directory invalid
	exit 1
fi

# Check if we have _any_ parameters
if [ -z $1 ];then
	show_usage "Create thumbnails for videos"
	exit 1
fi

# Next make sure that we have ffmpeg installed
if [ -z "`type ffmpeg`" ];then
	echo ffmpeg is not installed! This is required for generating thumbnails
	exit 1
fi

vid_base_name=`basename "$vid_path"`
vid_extension="${vid_base_name##*.}"
thumbnail_base_name="$vid_base_name.jpg"

# Always overwrite, grab first second first frame from $vid_path to
# nail_dir/vid_base_name.jpg
ffmpeg -y -ss 00:00:01 -i "$vid_path" -frames:v 1 "$nail_dir/$thumbnail_base_name"
