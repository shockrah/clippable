#!/bin/bash

set -e -x

pushd ts
	npm run build
popd

cargo build --release

mkdir -p build/

cp target/release/clippable-svc  build/server
cp clippable-svc/templates/ build/ -r
cp clippable-svc/static/ build -r

bash ./scripts/default-rocket-toml.sh
docker build -t registry.gitlab.com/shockrah/clippable .
