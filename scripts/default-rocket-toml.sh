#!/bin/sh

echo Copying in default configuration for web server
cat << EOF > build/Rocket.toml
[release]
address = "0.0.0.0"
port = 8482
keep_alive = 5
read_timeout = 5
write_timeout = 5
log_level = "critical"
limits = { forms = 4096 }

# Must be type <integer> (below is not valid toml syntax)
#workers = [number_of_cpus * 2]
EOF
