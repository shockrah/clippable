#!/bin/sh

# Label's a give 
msg='No Thumbnail found'
color="fontcolor=white"
size='fontsize=45'
pos='(w-text_w)/2'

rm -f out.gif
ffmpeg -i "$1" -vf \
	"drawtext=text='$msg':$color:$size:x=$pos:y=10:"\
	out.gif
