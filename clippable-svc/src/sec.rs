#[cfg(feature = "admin")]
// This module concerns itself with the encrypting/decrypting of passwords
// as well as the storage of those items
use rocket::tokio::io::AsyncReadExt;
use rocket::tokio::fs;

async fn random_string() -> Result<String, std::io::Error> {
    // First we read in some bytes from /dev/urandom
    let mut handle = fs::File::open("/dev/urandom").await?;
    let mut buffer = [0;32];
    handle.read(&mut buffer[..]).await?;

    Ok(base64::encode_config(buffer, base64::URL_SAFE_NO_PAD))
}


#[cfg(test)]
mod sec_api_tests {
    use rocket::tokio;
    use super::random_string;

    #[tokio::test]
    async fn generate_string() {
        println!("{:?}", random_string().await);
    }
}
