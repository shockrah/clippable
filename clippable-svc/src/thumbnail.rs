use rocket::fs::NamedFile;
use std::path::{Path, PathBuf};
use crate::common::thumbs_dir;

/// Returns a thumbnail whose formatted name is <video-name>.<ext>.jpg
#[get("/<file..>")]
pub async fn get(file: PathBuf) -> Option<NamedFile> {
    let clips_dir = thumbs_dir();

    // Only serve jpg's and png's through this route
    let file_path = Path::new(&clips_dir).join(file);
    if file_path.is_file() {
        return match file_path.extension() {
            Some(ext) => {
                match ext == "jpg" {
                    true => NamedFile::open(file_path).await.ok(),
                    false => None
                }
            },
            None => None
        }
    } else {
        let path = Path::new("static/cantfindshit.jpg");
        return NamedFile::open(path).await.ok();
    }
}
