use std::env;

/// Returns the absolute path to the videos directory
pub fn get_clips_dir() -> String {
    match env::var("CLIPS_DIR") {
        Ok(val) => val,
        Err(_) => "/media/clips/".to_string()
    }
}

/// Returns the absolute path to the thumbnails directory
pub fn thumbs_dir() -> String {
    match env::var("THUMBS_DIR") {
        Ok(val) => val,
        Err(_) => "/media/thumbnails/".to_string()
    }
}
