use std::path::PathBuf;

pub fn valid_filename(p: &PathBuf) -> bool {
    // Checks if a given filename is actually valid or not
    let mut valid = false;
    let s = p.file_name().unwrap_or_default().to_string_lossy();
    for e in [".mp4", ".webm", ".mkv"] {
        if s.ends_with(e) {
            valid = true;
            break;
        }
    }
    valid
}