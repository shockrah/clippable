#[cfg(feature = "admin")]

mod apikey;
mod response;
mod util;

use std::collections::HashMap;
use std::io::Result;
use std::fs;
use std::path::{Path, PathBuf};

use rocket::data::{Data, ToByteUnit};
use rocket::serde::json::Json;
use rocket_dyn_templates::Template;
use rocket::response::Redirect;
use response::{bad_request, ok};

use apikey::ApiKey;
use response::ActionResponse;
use crate::common::get_clips_dir;

#[get("/")]
pub async fn login_dashboard_redirect()  -> Redirect {
    Redirect::to("/admin/dashboard")
}

#[get("/dashboard")]
pub async fn login_dashboard() -> Template {
    // This page is basically just a login form
    // However the rest of the form is present on this page, just hidden
    let h: HashMap<i32,i32> = HashMap::new(); // does not allocate
    return Template::render("admin", &h);
}

#[post("/dashboard")]
pub async fn dashboard(_key: ApiKey) -> Json<ActionResponse> {
    // Assuming the api key check doesn't fail we can reply with Ok
    // at the application level
    ok()
}


#[post("/upload-video/<category>/<filename>", data = "<data>")]
pub async fn updload_video(_key: ApiKey, category: PathBuf, filename: PathBuf, data: Data<'_>) 
-> Result<Json<ActionResponse>> {
    /*
     * Uploads must have BOTH a valid filename and a category
     * Without the category the server will simply not find
     * the correct endpoint to reach and thus will 404
     */
    if util::valid_filename(&filename) == false {
        return Ok(bad_request(Some("Invalid filename(s)")));
    }

    let clips = get_clips_dir();
    fs::create_dir_all(Path::new(&clips).join(&category))?;

    /*
     * We allow up to 200 Megaytes per upload as most short
     * clips are not going to be very large anyway and this
     * should be a reasonably high limit for those that want
     * to upload "large" clips
     * */
    let filepath = Path::new(&clips).join(category).join(filename);
    data.open(250.megabytes()).into_file(filepath).await?;
    Ok(ok())
}

#[delete("/remove-video/<category>/<filename>")]
pub async fn remove_video(_key: ApiKey, category: PathBuf, filename: PathBuf) 
-> Result<Json<ActionResponse>> {
    let clips = get_clips_dir();
    let path = Path::new(&clips).join(&category).join(&filename);
    fs::remove_file(path)?;
    Ok(ok())
}
