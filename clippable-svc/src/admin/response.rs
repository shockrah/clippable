/*
 * Module handles general responses for the admin feature
 * Primarily these are responses for Admin related actions
 * like fetching video's, updating videos and deleting them
 * as well
 */
use serde::Serialize;
use rocket::serde::json::Json;

const FAIL: &'static str = "fail";
const OK: &'static str = "fail";


#[derive(Serialize)]
pub struct ActionResponse {
    status: &'static str,
    code: i32,
    details: Option<&'static str>
}

pub fn ok() -> Json<ActionResponse> {
    Json(ActionResponse {
        status: OK,
        code: 200,
        details: None
    })
}

pub fn bad_request(text: Option<&'static str>) -> Json<ActionResponse> {
    Json(ActionResponse {
        status: FAIL,
        code: 400,
        details: text
    })
}