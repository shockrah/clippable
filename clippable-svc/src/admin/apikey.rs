use rocket::request::{Outcome, Request, FromRequest};
use rocket::async_trait;
use rocket::http::Status;

use crate::db::{self, DB_PATH};

pub struct ApiKey {
    // These are used by rocket's driver code/decl macros however cargo
    // is not able to check those as the code is generated at compile time.
    // The dead code thing is just to stifle pointless warnings
    #[allow(dead_code)]
    uid: String,
    #[allow(dead_code)]
    key: String
}

#[derive(Debug)]
pub enum ApiKeyError {
    Missing,
    Invalid,
}


#[async_trait]
impl<'r> FromRequest<'r> for ApiKey {
    type Error = ApiKeyError;
    async fn from_request(req: &'r Request<'_>) -> Outcome<Self, Self::Error> {
        let key = req.headers().get_one("ADMIN-API-KEY");
        let uid = req.headers().get_one("ADMIN-API-UID");

        if key.is_none() || uid.is_none() {
            return Outcome::Failure((Status::Forbidden, ApiKeyError::Missing));
        }

        let (key, uid) = (key.unwrap(), uid.unwrap());

        println!("Path to use for db file {:?}", DB_PATH.to_string());
        let db = db::Database::load(DB_PATH.as_str().into()).unwrap();
        if let Some(stored) = db.get(uid) {
            if stored == key {
                return Outcome::Success(ApiKey {
                    key: key.into(),
                    uid: uid.into()
                })
            }
            return Outcome::Failure((Status::Forbidden, ApiKeyError::Invalid))
        }

        return Outcome::Failure((Status::Forbidden, ApiKeyError::Invalid))
    }
}
