/// This module takes care of returning bare webpage templates
/// Most of the work done here is simply to fill the templates
/// with the appropriate meta data that is required 

use rocket_dyn_templates::Template;
use rocket::fs::NamedFile;
use std::path::{Path, PathBuf};
use std::collections::HashMap;
use crate::common::get_clips_dir;
use std::env;

// TODO: try... literally try to reduce the number of clones that happen here

lazy_static! {
    static ref SITENAME: String = {
        env::var("SITE_NAME").unwrap_or_else(|_| "Clippable".to_string())
    };
    static ref SITEDESC: String = {
        env::var("SITE_DESC").unwrap_or_else(|_| "Short clips".to_string())
    };
    static ref SITEURL: String = {
        env::var("SITE_URL").unwrap_or_else(|_| "#".to_string())
    };
}

/// Default metadata that all web pages must be filled with
pub fn default_map() -> HashMap<&'static str, String> {
    let mut h: HashMap<&'static str, String> = HashMap::new();
    h.insert("sitetitle", SITENAME.clone());
    h.insert("sitedesc", SITEDESC.clone());
    h.insert("siteurl", SITEURL.clone());
    h
}

#[get("/")]
pub async fn home() -> Template {
    let mut h = default_map();
    h.insert("page", String::from("home"));
    h.insert("title", SITENAME.clone());

    Template::render("list", &h)
}

#[get("/category/<cat..>")]
pub async fn category(cat: PathBuf) -> Template {
    let mut h = default_map();
    h.insert("page", String::from("category"));

    // Opengraph meta tags bro
    let cat = cat.file_name().unwrap().to_string_lossy();
    h.insert("title", cat.to_string());
    h.insert("url", format!("/category/{}", cat));
    h.insert("desc", format!("{} clips", cat));
    h.insert("ogimg", format!("/thumbnail/{}/category-thumbnail.jpg", cat));

    Template::render("list", &h)
}

fn map_base_vfile(category: &Path, base: &Path) -> String {
    // This takes a category and base filename and basically looks for the file
    // that maps to those parameters
    // This basically aims to get rid of the extension in the URL so that social
    // media sites will bother to fetch open graph tags
    let basename = base.to_string_lossy();
    let dirname = format!("{}/{}/", get_clips_dir(), category.to_string_lossy());
    let dir = Path::new(&dirname);
    let file = dir.read_dir().ok().unwrap().find(|item| {
        if let Ok(item) = item {
            let name = item.file_name().into_string().unwrap();
            name.starts_with(basename.as_ref())
        } else {
            false
        }
    });
    match file {
        Some(ent) => ent.unwrap().file_name().into_string().unwrap(),
        None => "/static/cantfindshit.jpg".to_string()
    }
}

#[get("/clip/<cat>/<file_base>")]
pub fn video(cat: PathBuf, file_base: PathBuf) -> Template {
    let mut h: HashMap<&'static str, &str> = HashMap::new();

    // Look for the file_base + [extension]
    let file = map_base_vfile(&cat, &file_base);

    let cat = cat.to_string_lossy();

    let mut file_pretty = file.to_string();
    for c in [".mp4", ".mkv", ".webm"] {
        file_pretty = file_pretty.replace(c, "");
    }

    h.insert("title", &file_pretty);

    let url = format!("/clip/{}/{}", &cat, &file);
    h.insert("url", &url);
    h.insert("page", "video");
    h.insert("desc", &SITEDESC);
    h.insert("category", &cat);
    h.insert("filename", &file_pretty);

    let thumbnail = format!("/thumbnail/{}/{}", cat, file);
    h.insert("ogimg", &thumbnail);

    let clip_url = format!("/video/{}/{}", &cat, &file);
    h.insert("clip_url", &clip_url);

    let clip_thumbnail = format!("/thumbnail/{}/{}.jpg", &cat, &file);
    h.insert("clip_thumbnail", &clip_thumbnail);

    Template::render("video", &h)
}

/// This route handler returns static files that are comprised
/// of JS, CSS, the favicon and other static items
#[get("/<file..>")]
pub async fn files(file: PathBuf) -> Option<NamedFile> {
    NamedFile::open(Path::new("static/").join(file)).await.ok()
}
