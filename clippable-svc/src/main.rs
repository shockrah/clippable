//#![feature(decl_macro)]
#[macro_use] extern crate rocket;
#[macro_use] extern crate lazy_static;
use std::env;
use rocket_dyn_templates::Template;

mod page;
mod category;
mod video;
mod thumbnail;
mod common;

#[cfg(feature = "admin")]
mod admin;

#[cfg(feature = "admin")]
mod sec;

#[cfg(feature = "admin")]
mod db;

#[rocket::main]
async fn main() {
    // rid ourselves of random emoji's in logs
    env::set_var("ROCKET_CLI_COLORS", "false");


    if cfg!(feature = "admin") {
        #[cfg(feature = "admin")]
        let _ = rocket::build()
            .mount("/", routes![page::home, page::category, page::video]) // html
            .mount("/static", routes![page::files]) // files
            .mount("/api", routes![category::list, video::list]) // json
            .mount("/thumbnail", routes![thumbnail::get]) // images
            .mount("/video", routes![video::get_video]) // videos
            .mount("/admin", routes![
              admin::login_dashboard_redirect, 
              admin::login_dashboard, 
              admin::dashboard,
              admin::updload_video,
              admin::remove_video
            ])
            .attach(Template::fairing())
            .launch().await;
    } else {
        let _ = rocket::build()
            .mount("/", routes![page::home, page::category, page::video]) // html
            .mount("/static", routes![page::files]) // files
            .mount("/api", routes![category::list, video::list]) // json
            .mount("/thumbnail", routes![thumbnail::get]) // images
            .mount("/video", routes![video::get_video]) // videos
            .attach(Template::fairing())
            .launch().await;
    }
}
